/*
 * File: renderer.js
 *
 * Created: 16 jul 2018 15:38:46
 * Last Modified: 16 jul 2018 15:38:46
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React from 'react';
import {render} from 'react-dom';
import App from './components/App.jsx';
import './css/global.css';

render(<App />, document.getElementById('app'));
