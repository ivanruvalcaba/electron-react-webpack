/*
 * File: index.jsx
 *
 * Created: 16 jul 2018 16:03:15
 * Last Modified: 17 jul 2018 13:16:44
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {shell} from 'electron';
import styles from './styles.css';

export default class Link extends Component {
  static propTypes = {
    children: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
  };
  link(url) {
    shell.openExternal(url);
  }

  render() {
    console.log(styles);
    return (
      <a
        href="#"
        onClick={() => {
          this.link(this.props.to);
        }}
        className={styles.link}>
        {this.props.children}
      </a>
    );
  }
}
