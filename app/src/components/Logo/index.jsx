/*
 * File: index.jsx
 *
 * Created: 16 jul 2018 15:59:39
 * Last Modified: 16 jul 2018 15:59:39
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

export default class Logo extends Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
  };
  render() {
    return <img src={this.props.src} className={styles.logo} />;
  }
}
