/*
 * File: main.js
 *
 * Created: 16 jul 2018 15:41:15
 * Last Modified: 16 jul 2018 15:41:15
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

const electron = require('electron');
const {app, BrowserWindow} = electron;

// Let electron reloads by itself when webpack watches changes in ./app/
require('electron-reload')(__dirname);

// To avoid being garbage collected
let mainWindow;
let appIcon;

if (process.platform !== 'win32') {
  appIcon = `${__dirname}/app/src/assets/logo.png`;
} else {
  appIcon = `${__dirname}/app/src/assets/logo.ico`;
}

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    backgroudColor: '#f5f5f5',
    height: 600,
    icon: appIcon,
    minHeight: 600,
    minWidth: 800,
    title: 'MedicalMeet',
    width: 800,
    show: false
  });

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/app/index.html`);

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // Wait for 'ready-to-show' to displau our window
  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  // Install React Dev Tools
  const {
    default: installExtension,
    REACT_DEVELOPER_TOOLS
  } = require('electron-devtools-installer');

  installExtension(REACT_DEVELOPER_TOOLS)
    .then(name => {
      console.log(`Added Extension:  ${name}`);
    })
    .catch(err => {
      console.log('An error occurred: ', err);
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
