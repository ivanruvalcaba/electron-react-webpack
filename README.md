[//]: # (Filename: README.md)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <mario.i.ruvalcaba[at]gmail[dot]com>)
[//]: # (Created: 16 jul 2018 22:42:44)
[//]: # (Last Modified: 17 jul 2018 15:06:59)

# electron-react-webpack

Try this Electron & React 16 & Webpack 4 template for a quick development and prototyping.

![w10 sample](https://user-images.githubusercontent.com/11739632/37350993-59ad48d4-26da-11e8-9ac5-d3539cf1e2f9.PNG)

## Install

``` bash
# Clone the repository
$ git clone https://gitlab.com/ivanruvalcaba/electron-react-webpack

# Go into the repository
$ cd electron-react-webpack

# Install dependencies
$ npm install
```

or:

``` bash
# Clone the repository
$ git clone https://gitlab.com/ivanruvalcaba/electron-react-webpack

# Go into the repository
$ cd electron-react-webpack

# Install dependencies
$ yarn install
```

## Development

Just run this command to start developing with hot reloading.

``` bash
$ npm start
```

or:

``` bash
$ yarn start
```

## Production

``` bash
$ npm prod
```

or:

``` bash
$ yarn prod
```

## What's included

- JSX support for React.
- CSS modules support.
- JS, CSS and assets automatic bundling.
- Hot reloading via Webpack 4.
- Babel — ES Class Fields, Static Properties, Async/Await & Generator Functions support.
- ESLint & StyleLint support.
- Electron [React Developer Tools](https://github.com/facebook/react-devtools) build-in support.
- Split Webpack configuration files.

## Folder structure

```
├── electron-react-webpack/             # Your project's name, you can rename it

    ├── app/

        ├── build/                      # Webpack 4 will manage this folder for you
            ├── bundle.css              # Bundled CSS
            ├── bundle.js               # Bundled JS
            ├── ...                     # Your images will be copied here

        ├── src/

            ├── assets/                 # Images
                ├── electron.png
                ├── react.png
                ├── webpack.png
                ├── logo.png
                ├── logo.ico

            ├── components/             # React Components
                ├── Link/               # To keep them modularized follow this structure:
                    ├── index.jsx       # Your component's React code
                    ├── styles.css      # Your component's scoped CSS
                ├── Logo/
                    ├── index.jsx
                    ├── styles.css
                ├── App.jsx             # React main component where everything is tied up

            ├── renderer.js             # Electron's renderer-process, where you React app is called.
            ├── global.css              # Global CSS and global constants go here

        ├── index.html                  # This HTML only uses build/ folder's files

    ├── main.js                         # Electron's main process. Whole app is launched from here
    ├── package.json
    ├── webpack.dev.config.js           # Webpack 4 development setup
    ├── webpack.build.config.js         # Webpack 4 production setup
```

## License

This Source Code Form is subject to the terms of the **Mozilla Public License, v.2.0**. If a copy of the _MPL_ was not distributed with this file, You can obtain one at [http://mozilla.org/MPL/2.0/](http://mozilla.org/MPL/2.0/).
